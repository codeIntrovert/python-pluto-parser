from pluto_parser.transformer import UnitsTransformer
from pluto_parser import parser


def test_units_transformer(unit_example):
    """All unit examples parse back to their strings."""
    tree = parser.EngineeringUnitsParser.parse(unit_example)
    assert UnitsTransformer().transform(tree) == unit_example
