import glob

from pluto_parser import pluto_parse_file


for pluto_file in glob.glob("*.pluto"):
    pluto_parse_file(pluto_file)