# Python Pluto Parser

A Python module for parsing PLUTO scripts into native Python code.

PLUTO stands for *Procedure Language for Users in Test and Operations* and is a domain specific language (DSL). It was developed by the European Cooperation for Space Standardization (https://ecss.nl) for the automation of procedures in the development and utilization of space systems (but not limited to it). The PLUTO language is defined in standard [ECSS-E-ST-70-32C](docs/ECSS-E-ST-70-32C31July2008.pdf).

The advantage of using a DSL rather than general purpose programming language is that the syntax is typically easier to learn, less prone to errors, and focuses on the matter of the domain. In the case of PLUTO, this is the monitoring and control of systems, such as satellites, drones, test facilities, etc.

However, one needs to parse the DSL scripts into an executable code in order to run it. This is what this package is for.

## Getting Started

- Install the module:

  ```bash
  $ git clone https://gitlab.com/librecube/prototypes/python-pluto-parser
  $ cd python-pluto-parser
  $ python -m venv venv
  ```

Then activate install dependencies in Python virtual environment.

- On Linux:
  ```bash
  $ source venv/bin/activate
  (venv) $ pip install -e .
  ```

- On Windows:
  ```bash
  $ venv/bin/activate
  (venv) $ pip install -e .
  ```

- Write a PLUTO script, for example:

  ```
  /* example.pluto */
  procedure
    initiate and confirm step INIT
      initiate and confirm Activate of PowerSupply;
      wait for 2s;
      confirmation
        if Mode of PowerSupply = "ON"
      end confirmation
    end step;
  end procedure
  ```

Save this file as `example.pluto`.

- Running the script:
  ```bash
  $ pluto_parse example.pluto
  ```
  
  > To see how the generated how Python code looks for this example, run
  `pluto_parse example.pluto` directly in your terminal, which will create an `example.py` file. More examples can be found in the examples folder.

To learn more about how to write PLUTO procedures and how to define your system model, head to the [User Guide](docs/users.md).

## Contribute

- Issue Tracker: https://gitlab.com/librecube/prototypes/python-pluto-parser/-/issues
- Source Code: https://gitlab.com/librecube/prototypes/python-pluto-parser

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube guidelines](https://gitlab.com/librecube/guidelines).

## Support

If you are having issues, please let us know. Reach us at
[Matrix](https://app.element.io/#/room/#librecube.org:matrix.org)
or via [Email](mailto:info@librecube.org).

## License

The project is licensed under the MIT license. See the [LICENSE](./LICENSE.txt) file for details.
